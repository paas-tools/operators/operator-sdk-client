FROM centos:8

ENV RELEASE_VERSION=v1.5.0 \
    OKD4_VERSION=4.7.0-0.okd-2021-03-07-090821 \
    GO_VERSION=1.16.2 \
    BATS_VERSION=1.3.0\
    # Telling the container where to find the Go executable binaries
    PATH=$PATH:/usr/local/go/bin

RUN yum install -y ncurses gettext && \
    # Downloading operator-sdk
    curl -LO https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk_linux_amd64 && \
    # Downloading openshift-cli
    curl -L https://github.com/openshift/okd/releases/download/${OKD4_VERSION}/openshift-client-linux-${OKD4_VERSION}.tar.gz | tar xvz && \
    # Downloading go, needed by most commands of operator-sdk
    curl -L https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz | tar xvz -C /usr/local && \
    # Downlaoding bats, testing framework for shell
    curl -L https://github.com/bats-core/bats-core/archive/v${BATS_VERSION}.tar.gz | tar xvz && \
    # Installing bats
    ./bats-core-${BATS_VERSION}/install.sh /usr/local && rm -rf ./bats-core-${BATS_VERSION}/ && \
    # Setting up operator-sdk
    chmod +x operator-sdk_linux_amd64 &&  mkdir -p /usr/local/bin/ &&  cp operator-sdk_linux_amd64 /usr/local/bin/operator-sdk && rm operator-sdk_linux_amd64 && \
    # Setting up openshift-cli
    cp ./oc /usr/local/bin && rm oc

CMD ["/usr/local/bin/operator-sdk"]
